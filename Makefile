PROJECT_NAME = secrets_service
PIPENV_BIN=$(shell which pipenv)
INFRA_DIR = infra
BUILD_DIR = build
CURRENT_DIR=$(shell pwd)
DYNAMODB_TABLE=secrets-service

.DEFAULT: help
help:
	@echo "make run"
	@echo "	   run ${PROJECT_NAME} locally"
	@echo "make setup"
	@echo "	   run ${PIPENV_BIN} install -d"
	@echo "make build"
	@echo "	   copy the application and dependencies to directory: ${BUILD_DIR} "
	@echo "make package"
	@echo "	   build and zip up all related files in directory: ${BUILD_DIR} "
	@echo "make docs"
	@echo "	   generate the openapi spec file"
	@echo "make view_api"
	@echo "	   create a OpenAPI json file and run docker to view the documentation"
	@echo "make create_dynamo"
	@echo "	   create a DynamoDB table named ${DYNAMODB_TABLE} in LocalStack"
	@echo "make build package"
	@echo "	   copy all files and dependencies to ${BUILD_DIR} and create zip file"

setup: Pipfile
	${PIPENV_BIN} install -d

run:
	$(info Running ${PROJECT_NAME} locally!)
	FLASK_APP=${PROJECT_NAME}/app.py ${PIPENV_BIN} run flask run

build: FORCE
	$(info Building Lambda!)
	rm -rf ${BUILD_DIR}
	mkdir ${BUILD_DIR}
	cp -r lambda.py ${PROJECT_NAME} ${BUILD_DIR}
	${PIPENV_BIN} lock -r > ${BUILD_DIR}/requirements.txt
	cd ${BUILD_DIR} \
		&& pip install -r requirements.txt --no-deps -t .
FORCE:

package:
	$(info Packaging Lambda!)
	cd $(BUILD_DIR) && zip -r lambda.zip *

docs: FORCE
	$(info Generating openapi.json spec file!)
	${PIPENV_BIN} run python ./generate_swagger.py --out openapi.json

clean:
	$(info Cleaning up!)
	rm -rf ${BUILD_DIR} .pytest_cache htmlcov .coverage

view_api: docs
	$(info running Docker to view OpenAPI docs)
	$(info point your browser to http://localhost:8082 to see your OpenAPI documentation)
	docker run -p 8082:8080 -e SWAGGER_JSON=/bar/openapi.json -v "${CURRENT_DIR}":/bar swaggerapi/swagger-ui

create_dynamo: FORCE
	$(info Creating DynamoDB table!)
	aws dynamodb --endpoint-url=http://localhost:4569 create-table \
		--region us-east-1 \
		--table-name ${DYNAMODB_TABLE} \
		--attribute-definitions AttributeName=uuid,AttributeType=S \
		--key-schema AttributeName=uuid,KeyType=HASH \
		--billing-mode PAY_PER_REQUEST

	aws dynamodb --endpoint-url=http://localhost:4569 list-tables \
		--region us-east-1
